# Scripts for analyzing CodaLab submissions

There is a chance that some of these are re-discovering the wheel, but
it was faster to put together these than look for already existing
solutions.

In general, there are currently two scripts here:

- `download-results.py` downloads all the results from a codalab
  competition. For it to work, you should the username and password of
  the competition administrator in `codalab-credentials.txt`, and run
  with the competition number as the parameter. By default, it creates
  a file `metadata.csv`, which contains the metadata extracted from
  CodaLab, and a directory `submissions` which contains all valid
  submission (zip) files.
- `leaderboard.py` takes the `metadata.csv` and the submission
  downloaded by `download-results.py`, and outputs a leaderboard with
  some more information - macro-averaged precision, recall, f-score,
  as well as binary (taking `OFF` as positive label) scores. The
  results are written to `leaderboard.csv`. There is also a very
  experimental code for producing MCMC-based p-values, but it is slow,
  and not tested.

Both scripts are written in Python 3 and some standard libraries.
There is some basic command-line argument `--help`.
