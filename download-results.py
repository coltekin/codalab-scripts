#!/usr/bin/env python3

import sys, os, argparse, csv, requests
from lxml import etree
from dateutil import parser

SITE_URL="https://competitions.codalab.org"
LOGIN_URL= SITE_URL + "/accounts/login/"

argp = argparse.ArgumentParser()
argp.add_argument('competition',
        help="The competition number ")
argp.add_argument('--credentials', '-c', default='codalab-credentials.txt')
argp.add_argument('--output-dir', '-O', default='.')
argp.add_argument('--download-failed', '-F', action='store_true')
argp.add_argument('--verbose', '-v', action='store_true')
args = argp.parse_args()

sub_url= SITE_URL + "/my/competition/{}/submissions/".format(args.competition)
sub_fmt = SITE_URL + '/my/competition/submission/{}/input.zip'

submission_dir = os.path.join(args.output_dir, 'submissions')
os.makedirs(submission_dir, exist_ok=True)

with open(args.credentials, 'r') as fp:
    csvr = csv.reader(fp, delimiter='\t')
    user, passwd = next(csvr)
if user == 'username':
    print("Please place your CodaLab username/password in {}.".format(
        args.credentials))
    sys.exit(-1)

with requests.Session() as s:
    r = s.get(LOGIN_URL)
    html = etree.HTML(r.content)
    e = html.xpath('//input[@name="csrfmiddlewaretoken"]')[0]
    payload = {'login': user, 
               'password': passwd,
               'csrfmiddlewaretoken': e.get('value')}
    s.headers.update({'referer': LOGIN_URL})
    p = s.post(LOGIN_URL, payload)

    metadata = list()
    r = s.get(sub_url, allow_redirects=True)
    while r:
        html = etree.HTML(r.content)
        submission_rows = html.xpath('//tr[@class="finished-submission"]')
        if args.download_failed:
            submission_rows += html.xpath('//tr[@class="failed-submission"]')
        for tr in submission_rows:
            status = tr.get('class', 'unknown')
            _, t, u, sid, slink, stat, _ , f1, _, _, _, _, _ = tr.xpath('./td')
            u, f1 = u.text, f1.text
            t = parser.parse(t.text.replace('midnight', '0:00')).isoformat()
            sid = sid.text
            slink = slink.getchildren()[0].get('href')
            submission = s.get(SITE_URL + slink)
            sub_file = os.path.join(submission_dir, sid + '.zip') 
            with open(sub_file, 'wb') as fp:
                if args.verbose:
                    print("Downloading submission {}.".format(sid))
                fp.write(submission.content)
            metadata.append((t, u, sid, sub_file, f1, status))
        pag = html.xpath('//ul[@class="pagination"]/li/a[text() = "next"]')
        if pag:
            r = s.get(sub_url + pag[0].get('href'), allow_redirects=True)
        else:
            r = None

    with open(os.path.join(args.output_dir, 'metadata.csv'), 'wt') as fp:
        csw = csv.writer(fp)
        csw.writerow(('time', 'user', 'sub_id', 'sub_file', 'f1', 'status'))
        for row in metadata:
            csw.writerow(row)

