#!/usr/bin/env python3

import csv, argparse, sys, io, time, os
from zipfile import ZipFile
from sklearn.metrics import precision_recall_fscore_support as prfs
from sklearn.metrics import confusion_matrix, f1_score
import numpy as np
import itertools
import multiprocessing as mp


def mcmc_interval(pred, gold, interval=95, samples=10, score='f1', average='macro'):
    known_scores = ['precision', 'recall', 'f1']
    pred = np.array(pred)
    gold = np.array(gold)
    sc_list = list()
    for _ in range(samples):
        i = np.random.choice(len(pred), size=len(pred))
        scores = prfs(gold[i], pred[i], average=average)
        sc_list.append(scores[known_scores.index(score)])
    return tuple(np.percentile(sc_list, 
                [100-interval, interval], overwrite_input=True))


def mcmc_pvalue(pred_up, pred_down, delta, samples=10):
    pred_up, pred_down = np.array(pred_up), np.array(pred_down)
    n_samples = int(np.floor(samples ** 0.5))
    sample = np.full((len(pred_up), n_samples), pred_up[0])
    n = len(pred_up)

    t = time.time()
    for j in range(n_samples):
        r = np.random.random(size=len(pred_up)) > 0.5
        s = sample[:,j]
        s[r] = pred_up[r]
        s[np.logical_not(r)] = pred_down[np.logical_not(r)]

    p = 0
    fscores = []
    for i in range(n_samples):
        for j in range(n_samples):
            if i == j: continue
            fscores.append(f1_score(sample[:,i], sample[:,j], average='macro'))
#    print('fscores', len(fscores), time.time() - t)
#    t = time.time()
    fscores = np.array(fscores)
    dx = fscores[..., np.newaxis] - fscores[np.newaxis, ...]
#    print('distance', dx.shape, time.time() - t)
    dx = dx.ravel()
    return sum(dx > delta) / (len(dx) - n_samples)

argp = argparse.ArgumentParser()
argp.add_argument('metadata',  help='The output of download-submissions.py')
argp.add_argument('goldstandard', help='File with reference labels.')
argp.add_argument('--check-best', '-c', action='store_true')
argp.add_argument('--calculate-significance', '-s', action='store_true')
argp.add_argument('--calculate-interval', '-i', action='store_true')
argp.add_argument('--mcmc-samples', '-S', type=int, default=100)
argp.add_argument('--use-best', '-b', action='store_true')
argp.add_argument('--leaderboard', '-l', default='leaderboard.csv', help="Leaderboard file name")
argp.add_argument('--processes', '-j', type=int, default=4)
argp.add_argument('--team-user-mapping', '-t', default="team-user-mapping.txt")
args = argp.parse_args()

metadata = list()
with open(args.metadata, 'r') as fp:
    csvr = csv.DictReader(fp)
    for row in csvr:
        metadata.append(row)

goldstd = dict()
with open(args.goldstandard, 'r') as fp:
    for line in fp:
        id_, label = line.strip().split(',')
        goldstd[id_] = label

user_team = dict()
if os.path.exists(args.team_user_mapping):
    with open(args.team_user_mapping, 'r') as fp:
        _ = next(fp)
        for line in fp:
            team, user = line.strip('\r\n ').split('\t')
            user_team[user] = team

user_dict = dict()
gold_ids = sorted(goldstd)
gold_labels = [goldstd[k] for k in gold_ids]

#time,user,sub_id,sub_file,f1
for meta in metadata:
    with ZipFile(meta['sub_file']) as zip_file:
        csvfile = [f for f in zip_file.namelist() if f.endswith('.csv') and not '/' in f]
        if len(csvfile) > 1:
            print("!!! {} multiple csv files: {}".format(meta['sub_id'], csvfile))
        csvfile = csvfile[0]
        with zip_file.open(csvfile, mode='r') as fp:
            text = io.TextIOWrapper(fp, newline=None)
            pred = dict([x.split(',') for x in text.read().strip().split('\n')])
        if meta['user'] not in user_dict:
            user_dict[meta['user']] = dict()
        user_dict[meta['user']][meta['sub_id']] = pred

# print the leaderboard
score_labels = ("id precision-macro recall-macro f1-macro tp fp fn tn "
                "precision-binary recall-binary f1-binary").split()
for user, predictions in user_dict.items():
    if args.check_best:
        best_f1, best_id, best_sc = 0, None, list()
    for sub_id, pred in predictions.items():
        labels = [pred[k] for k in gold_ids]
        p, r, f, _ = prfs(gold_labels, labels, average='macro', warn_for={})
        tn, fp, fn, tp = confusion_matrix(gold_labels, labels).ravel()
        bp, br, bf, _ = prfs(gold_labels, labels, average='binary',
                             pos_label='OFF', warn_for={})
        if args.check_best and f > best_f1:
            best_f1 = f
            best_id = sub_id
            best_sc = (sub_id, p, r, f, tp, fp, fn, tn, bp, br, bf)
        last_sc = (sub_id, p, r, f, tp, fp, fn, tn, bp, br, bf)
    if args.check_best:
        print("Last ({}) is not best ({}) for user {}.".format(
            last_sc[0], bset_sc[0], user), file=sys.stderr)
    if args.use_best:
        last_sc = best_sc
        sub_id = best_id
    user_dict[user]['best'] = sub_id
    user_dict[user]['scores'] = dict(zip(score_labels, last_sc))

rank = [u for u in sorted(user_dict,
            key=lambda x: user_dict[x]['scores']['f1-macro'], reverse=True)]

fmt = "{},{},{},{}" + ",{}" * 3 + ",{}" * 4 + ",{}" * 3 + ",{}" * 3
with open(args.leaderboard, 'wt') as fp:
    print(("user,team" + ",{}" * 12 + ",intbelow,intabove,signif").format(
        *score_labels), file=fp)

    def sampling_scores(rank_user,
            calculate_interval=args.calculate_interval,
            calculate_significance=args.calculate_significance):
        i, user = rank_user
        signif = 'NA'
        interval = ('NA', 'NA')
        if args.calculate_interval:
            sub_id = user_dict[user]['scores']['id']
            pred = user_dict[user][sub_id]
            interval = mcmc_interval([pred[k] for k in gold_ids],
                    gold_labels, samples=args.mcmc_samples)
        if args.calculate_significance:
            if i < len(rank) - 1:
                u_below = rank[i + 1]
                print(user, u_below, flush=True)
                sub_id = user_dict[user]['scores']['id']
                sub_below = user_dict[u_below]['scores']['id']
                pred = user_dict[user][sub_id]
                pred_below = user_dict[u_below][sub_below]
                f1 = user_dict[user]['scores']['f1-macro']
                f1_below = user_dict[u_below]['scores']['f1-macro']
                signif = mcmc_pvalue([pred[k] for k in gold_ids],
                    [pred_below[k] for k in gold_ids],
                    f1 - f1_below, samples=args.mcmc_samples)
        return interval[0], interval[1], signif

    pool = mp.Pool(processes=args.processes)
    stats = pool.map(sampling_scores, [(i, user) for i, user in enumerate(rank)])

    for i, user in enumerate(rank):
        scores = [user_dict[user]['scores'][sc] for sc in score_labels]
        scores.extend(stats[i])
        print(fmt.format(i + 1, user, user_team.get(user, user), *scores), file=fp)
