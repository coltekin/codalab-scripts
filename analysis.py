#!/usr/bin/env python3

import csv, argparse, sys, io, time
from zipfile import ZipFile
from sklearn.metrics import precision_recall_fscore_support as prfs
from sklearn.metrics import confusion_matrix, f1_score
import numpy as np
import itertools
from collections import Counter

argp = argparse.ArgumentParser()
argp.add_argument('metadata',  help='The output of download-submissions.py')
argp.add_argument('goldstandard', help='File with reference labels.')
argp.add_argument('--team-user-mapping', '-t')
argp.add_argument('--score-cutoff', '-c', type=float)
args = argp.parse_args()

metadata = list()
with open(args.metadata, 'r') as fp:
    csvr = csv.DictReader(fp)
    for row in csvr:
        metadata.append(row)

goldstd = dict()
with open(args.goldstandard, 'r') as fp:
    for line in fp:
        id_, label = line.strip().split(',')
        goldstd[id_] = label

user_team = dict()
if args.team_user_mapping:
    with open(args.team_user_mapping, 'r') as fp:
        _ = next(fp)
        for line in fp:
            team, user = line.strip().split('\t')
            user_team[user] = team

user_dict = dict()
gold_ids = sorted(goldstd)
gold_labels = [goldstd[k] for k in gold_ids]

#time,user,sub_id,sub_file,f1
for meta in metadata:
    if args.score_cutoff and args.score_cutoff > float(meta['f1']):
        continue
    with ZipFile(meta['sub_file']) as zip_file:
        csvfile = [f for f in zip_file.namelist() if f.endswith('.csv') and not '/' in f]
        if len(csvfile) > 1:
            print("!!! {} multiple csv files: {}".format(meta['sub_id'], csvfile))
        csvfile = csvfile[0]
        with zip_file.open(csvfile, mode='r') as fp:
            text = io.TextIOWrapper(fp, newline=None)
            pred = dict([x.split(',') for x in text.read().strip().split('\n')])
        if meta['user'] not in user_dict:
            user_dict[meta['user']] = dict()
        user_dict[meta['user']][meta['sub_id']] = pred

n_user = len(user_dict)

testset_correct = {k:0 for k in goldstd}
n = 0
for user in user_dict:
    for sub, pred in user_dict[user].items():
        label_dist = Counter(pred.values())
        if len(label_dist) < 2:
            print("Skipping {}({})/{}. All labels are {}.".format(
                user_team.get(user, user),
                user, sub, list(label_dist.keys())[0]))
            continue
        n += 1
        for k in goldstd:
            if goldstd[k] == pred[k]:
               testset_correct[k] += 1
               
majority = dict()
oracle = dict()
for k in goldstd:
    if testset_correct[k] > n_user/2:
        majority[k] = goldstd[k]
    elif goldstd[k] == 'OFF':
        majority[k] = 'NOT'
    elif goldstd[k] == 'NOT':
        majority[k] = 'OFF'
    if testset_correct[k] != 0:
        oracle[k] = goldstd[k]
    elif goldstd[k] == 'OFF':
        oracle[k] = 'NOT'
    elif goldstd[k] == 'NOT':
        oracle[k] = 'OFF'

print('Oracle scores:', prfs([goldstd[k] for k in gold_ids],
    [oracle[k] for k in gold_ids], average='macro'))
print('Majority scores (n={}): {}, {}, {}, {}'.format(
    n_user, *prfs([goldstd[k] for k in gold_ids],
    [majority[k] for k in gold_ids], average='macro')))


n_low = 0
for k, v in testset_correct.items():
#    if v/n < 0.05:
    if v < 1:
        print("{} {} {:02.2f}".format(k, goldstd[k], 100*v/n))
        n_low += 1

c = Counter(testset_correct.values())

print(n, n_low)
